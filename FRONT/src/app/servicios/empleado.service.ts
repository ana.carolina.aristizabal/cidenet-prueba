import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  private BASE_URL = 'api/empleados/';
  constructor(private http:HttpClient) { 


  }
  getEmpleados():Observable<any>{
    return this.http.get(`${this.BASE_URL}`)
  }
  crearEmpleado(empleado:Object):Observable<any>
  {
    return this.http.post(`${this.BASE_URL}` , empleado);
  }
}

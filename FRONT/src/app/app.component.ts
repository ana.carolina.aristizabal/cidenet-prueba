import { Component } from '@angular/core';
import { Empleado } from './modelos/empleado';
import { EmpleadoService } from './servicios/empleado.service';
import { DatePipe, formatDate } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  empleadoArray:Empleado[] = [];
  constructor(private empleadoService:EmpleadoService){
    
  }
  ngOnInit():void{
    this.selectedEmpleado.estado='Activo'
    this.empleadoService.getEmpleados()
    .subscribe(data=>{
      console.log(data)
      this.empleadoArray = data
      console.log(this.empleadoArray)
    ,
  error=>console.log(error);
})
  }

  selectedEmpleado:Empleado = new Empleado();
seleccionar(empleado:Empleado){
  this.selectedEmpleado = empleado;
}

nuevo(){
  this.selectedEmpleado=new Empleado();
}
guardar(){
  if(this.selectedEmpleado.id == null){
    console.log(this.selectedEmpleado)
    this.empleadoService.crearEmpleado(this.selectedEmpleado)
    .subscribe(data => {
      console.log(data)
      if(data.transaccion){
        this.empleadoService.getEmpleados()
      .subscribe(data => {
        console.log(data)
        this.empleadoArray = data;
      }, error => console.log(error));
     }
    })
  }
  else{
    console.log("caso editar");
  }
  this.ngOnInit();
}

validacionCadena():string {

  if(this.selectedEmpleado.primer_apellido!=undefined){
    console.log(this.selectedEmpleado.primer_apellido.length)
    if( this.selectedEmpleado.primer_apellido.length > 20) {
          return "Cadena muy larga";
    }
  }
      return ""
  
}

validacionNumid():string {

  if(this.selectedEmpleado.num_id!=undefined){
    console.log(this.selectedEmpleado.num_id.length)
    if( this.selectedEmpleado.num_id.length > 20) {
          return "Cadena muy larga";
    }
  }
      return ""
  
}

correo():string {

  if(this.selectedEmpleado.pais!=undefined){
    if(this.selectedEmpleado.pais == 'Colombia'){
      this.selectedEmpleado.correo = this.selectedEmpleado.primer_nombre+this.selectedEmpleado.primer_apellido+'@cidenet.com.co';
      return this.selectedEmpleado.correo.replace(/\s/g, "");
    }

    if(this.selectedEmpleado.pais == 'Estados_unidos'){
      this.selectedEmpleado.correo = this.selectedEmpleado.primer_nombre+this.selectedEmpleado.primer_apellido+'@cidenet.com.us';
      return this.selectedEmpleado.correo.replace(/\s/g, "");
    }

  }
  
}

fecha():string {

  if(this.selectedEmpleado.fecha_ingreso!=undefined){
    const hoy = Date.now(); 
    const fech = formatDate(hoy, 'yyyy-MM-dd', 'en-US')
    const fecha_ingreso = formatDate(this.selectedEmpleado.fecha_ingreso, 'yyyy-MM-dd', 'en-US')
    console.log(fech);
    console.log(this.selectedEmpleado.fecha_ingreso);
    if(fecha_ingreso > fech){
      return "La fecha de ingreso no puede ser mayor"

  }
}
    return ""
  }
  
}



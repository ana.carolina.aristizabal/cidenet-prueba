export class Empleado {
    id:BigInteger;
    num_id:BigInteger;
    primer_apellido :string;
    segundo_apellido :string;
    primer_nombre :string;
    otros_nombres:string;
    pais:string;
    tipo_id :string;
    correo :string;
    fecha_ingreso:Date;
    area :string;
    estado :string;
    fecha_registro:Date;

}

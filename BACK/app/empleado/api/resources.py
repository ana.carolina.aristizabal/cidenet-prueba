from flask import request, Blueprint
from flask.json import jsonify
from flask_restful  import Api, Resource
from datetime import datetime
from marshmallow import exceptions
from app.common.error import valores
from .schemas import EmpleadoSchema
from http import HTTPStatus
from ..models import empleados
emps_v1_0_bp = Blueprint('emps_v1_0_bp', __name__)
emp_schema = EmpleadoSchema()
valor = valores()
api = Api(emps_v1_0_bp)
class EmpListResource(Resource):
    def get(self):
        empl = empleados.get_all()
        result = emp_schema.dump(empl, many=True)
        return result , 200
    
    def post(self):
        data = request.get_json()
        try:
            emp_dict = emp_schema.load(data)
            emp = empleados(num_id= emp_dict['num_id'],
                        primer_apellido=emp_dict['primer_apellido'],
                        segundo_apellido=emp_dict['segundo_apellido'],
                        primer_nombre=emp_dict['primer_nombre'],
                        otros_nombres=emp_dict['otros_nombres'],
                        pais=emp_dict['pais'],
                        tipo_id=emp_dict['tipo_id'],
                        fecha_ingreso=emp_dict['fecha_ingreso'],
                        correo=emp_dict['correo'],
                        fecha_registro=None ,
                        estado=emp_dict['estado'],
                        area=emp_dict['area']
            )       
            resp = valores.validaciones(data)
            emp.save()
            return resp
        except exceptions.ValidationError as err:
            return jsonify(err.messages,400)

api.add_resource(EmpListResource, '/api/empleados/', endpoint='emp_list_resource')
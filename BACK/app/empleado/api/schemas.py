from marshmallow import fields
from app.ext import ex
class EmpleadoSchema(ex.Schema):
    id = fields.Integer()
    num_id = fields.Integer()
    primer_apellido = fields.String()
    segundo_apellido = fields.String()
    primer_nombre = fields.String()
    otros_nombres = fields.String()
    pais = fields.String()
    tipo_id = fields.String()
    fecha_ingreso = fields.Date('%Y-%m-%d')
    correo = fields.String()
    area = fields.String()
    estado = fields.String()
    fecha_registro = fields.DateTime(allow_none=True)
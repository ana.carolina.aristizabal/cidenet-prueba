from app.db import db, BaseModelMixin
from sqlalchemy import  text,Column,TIMESTAMP
from datetime import datetime
class empleados(db.Model, BaseModelMixin):
    id = db.Column(db.Integer, primary_key=True)
    num_id = db.Column(db.Integer)
    primer_apellido = db.Column(db.String(100))
    segundo_apellido = db.Column(db.String(100))
    primer_nombre = db.Column(db.String(100))
    otros_nombres = db.Column(db.String(100))
    pais = db.Column(db.String(100))
    tipo_id = db.Column(db.String(100))
    correo = db.Column(db.String(100))
    fecha_ingreso = db.Column(db.Date)
    area = db.Column(db.String(100))
    estado = db.Column(db.String(100))
    fecha_registro = Column('fecha_registro', TIMESTAMP(timezone=False), nullable=False, default=datetime.now())
    def __init__(self, num_id, primer_apellido, segundo_apellido, primer_nombre,otros_nombres,pais,tipo_id,correo,fecha_ingreso,area,estado,fecha_registro):
        self.num_id = num_id
        self.primer_apellido = primer_apellido
        self.segundo_apellido = segundo_apellido
        self.primer_nombre = primer_nombre
        self.otros_nombres = otros_nombres
        self.pais = pais
        self.tipo_id = tipo_id
        self.correo = correo
        self.fecha_ingreso = fecha_ingreso
        self.area = area
        self.estado = estado
        self.fecha_registro = fecha_registro
    def __repr__(self):
        return f'empleados({self.num_id})'
    def __str__(self):
        return f'{self.num_id}'